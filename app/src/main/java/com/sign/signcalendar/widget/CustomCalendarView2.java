package com.sign.signcalendar.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.sign.signcalendar.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *  自定义日历界面 签到
 * Created by six.sev on 2017/3/2.
 */

public class CustomCalendarView2 extends View {
    private static final int WEEK_NUM = 7;
    //drawtext的误差Y值
    private static final int offsetY = 6;

    //默认背景色
    private int defaultBgColor = 0xffffffff;
    //默认字体色
    private int defaultTextColor = 0xff000000;
    //默认字体大小
    private int defaultTextSize = 28;
    //默认行间距
    private int defaultLineSpace = 10;

    //圆圈背景半径
    private int radius = 40;
    //主背景色
    private int mainBgColor;
    //月份的背景色
    private int monthBgColor;
    //月份的背景色
    private int weekBgColor;
    //日期的背景色
    private int dayBgColor;
    //月份字体大小
    private int monthTextSize;
    //日期字体大小
    private int dayTextSize;
    //月份的颜色
    private int monthTextColor;
    //日期的字体颜色
    private int dayTextColor;
    //星期的颜色
    private int weekTextColor;
    //星期的字体大小
    private int weekTextSize;
    //签到字体的颜色
    private int signDayTextColor;
    //签到日期的背景色
    private int signDayBgColor;

    //字体上下间距
    private int textPadding;
    //行间距
    private int lineSpace;

    //月份的高度
    private int monthHeight;
    //星期的高度
    private int weekHeight;
    //一个日期的高度
    private int dayHeight;

    //当前是几号
    private int currentDay;
    //选中
    private int selectDay;
    //当前是否为本月
    private boolean isCurrentMonth = false;
    //这周的第几天。
    private int week;
    //这月有多少天
    private int dayNums;
    //第一行从星期几开始排列, 这个星期的第几天。 从星期天开始算。
    private int firstLineIndex;
    //第一行可以显示多少个
    private int firstLineNums;
    //总共需要分几行
    private int lineNums;
    //最后一行要显示的个数
    private int lastLineNums;
    //画笔
    private Paint paint;
    //当前设置的日期
    private Date mineMonth;
    //顶部起始坐标
    private int top = 0;
    //列宽
    int columnWidth;
    //上下文
    private Context context;
    //左边图标的左坐标
    private int leftIcon_left;
    //右边图标的左坐标
    private int rightIcon_left;
    private int iconAreaWidth;
    //当前的最新显示月份
    private String mMonth;
    private Calendar mCalendar;
    //一个月的日期数组true为已签到， false为未签到
    private List<Boolean> bgList;

    //显示汉字微调的距离
    private float charOffset;

    public CustomCalendarView2(Context context) {
        this(context, null);
    }

    public CustomCalendarView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        defaultTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.default_calendar_textsize), getResources().getDisplayMetrics());
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomCalendarView2);
        mainBgColor = a.getColor(R.styleable.CustomCalendarView2_mainBgColor, defaultBgColor);
        monthBgColor = a.getColor(R.styleable.CustomCalendarView2_monthBgColor, defaultBgColor);
        weekBgColor = a.getColor(R.styleable.CustomCalendarView2_weekBgColor, defaultBgColor);
        dayBgColor = a.getColor(R.styleable.CustomCalendarView2_dayBgColor, defaultBgColor);
        monthTextColor = a.getColor(R.styleable.CustomCalendarView2_monthTextColor, defaultTextColor);
        monthTextSize = a.getDimensionPixelSize(R.styleable.CustomCalendarView2_monthTextSize, defaultTextSize);
        dayTextColor = a.getColor(R.styleable.CustomCalendarView2_dayTextColor, defaultTextColor);
        dayTextSize = a.getDimensionPixelSize(R.styleable.CustomCalendarView2_dayTextSize, defaultTextSize);
        weekTextColor = a.getColor(R.styleable.CustomCalendarView2_weekTextColor, defaultTextColor);
        weekTextSize = a.getDimensionPixelSize(R.styleable.CustomCalendarView2_weekTextSize, defaultTextSize);

        signDayTextColor = a.getColor(R.styleable.CustomCalendarView2_signDayTextColor, defaultTextColor);
        signDayBgColor = a.getColor(R.styleable.CustomCalendarView2_signDayBgColor, defaultTextColor);


        lineSpace = a.getDimensionPixelSize(R.styleable.CustomCalendarView2_lineSpace, defaultLineSpace);
        textPadding = a.getDimensionPixelSize(R.styleable.CustomCalendarView2_textPadding, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.default_calendar_textpadding), getResources().getDisplayMetrics()));
        a.recycle();

        charOffset = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = /*monthHeight + */weekHeight + dayHeight * lineNums + textPadding * lineNums;
        setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec), height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //drawMonth(canvas);
        drawWeek(canvas);
        drawDay(canvas);
    }

    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true);
        /*paint.setTextSize(monthTextSize);
        monthHeight = getFontHeight(paint) + textPadding;*/
        monthHeight = 0;

        paint.setTextSize(weekTextSize);
        weekHeight = getFontHeight(paint) + textPadding;

        paint.setTextSize(dayTextSize);
        dayHeight = getFontHeight(paint);
        dayHeight += textPadding * 2;
        radius = dayHeight / 3;
    }

    /**
     *通过传入的日期字符串来获取对应的排列参数
     * @param month
     */
    public void setMonth(String month){
        mineMonth = string2Date(month);
        lineNums =0;

        mCalendar = Calendar.getInstance();
        mCalendar.setTime(new Date());
        currentDay = mCalendar.get(Calendar.DAY_OF_MONTH);

        Date temp = string2Date(date2String(new Date()));
        if(temp.getTime() == mineMonth.getTime()){
            isCurrentMonth = true;
            selectDay = currentDay;
        }else{
            isCurrentMonth = false;
            selectDay = 0;
        }

        mCalendar.setTime(mineMonth);
        dayNums = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        week = mCalendar.get(Calendar.DAY_OF_WEEK);
        firstLineIndex = week - 1;
        firstLineNums = WEEK_NUM - firstLineIndex;
        lineNums++;

        int left = dayNums - firstLineNums;
        while (left > 7){
            lineNums++;
            left -= 7;
        }
        if(left > 0){
            lineNums++;
            lastLineNums = left;
        }
    }

    /**
     * 传入参数为boolean类型的数组，表明当前日期是否为已签到的
     * @param bgList
     */
    public void setDayBgList(List<Boolean> bgList){
        this.bgList = bgList;
    }

    public void refresh(){
        requestLayout();
        invalidate();
    }

    /**
     * 获取的系统时间转换为我们需要的字符串格式
     * @param date
     * @return
     */
    public String date2String(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月");
        return format.format(date);
    }

    /**
     * 画月份
     * @param canvas
     */
    private void drawMonth(Canvas canvas) {
        paint.setColor(monthBgColor);
        RectF rect = new RectF(0, 0, getWidth(), monthHeight);
        canvas.drawRect(rect, paint);
        Log.d("liuqi", "monthHeight = " + monthHeight);
        String month = date2String(mineMonth);
        paint.setTextSize(monthTextSize);
        paint.setColor(monthTextColor);
        int monthWidth = getFontLength(paint, month);
        int left = (getWidth() - monthWidth) / 2;
        int baseline = (monthHeight + getFontHeight(paint)) / 2 - offsetY ;
        //x是指字符距离屏幕左边的距离， 但如果设置textalign = center则是距离中心点的位置
        //y是指text的baseline(文字除字母g f 等超出的外 其他字符的最底部所在的位置)的位置postion
        canvas.drawText(month, left, baseline , paint);

        /*Resources resources = context.getResources();
        Bitmap icon = BitmapFactory.decodeResource(resources, R.mipmap.back);
        int iconWidth = icon.getWidth();
        int iconHeight = icon.getHeight();
        leftIcon_left = left - iconWidth - textPadding;
        int iconTop = (monthHeight - iconHeight)/2;
        iconAreaWidth = iconWidth + textPadding;
        canvas.drawBitmap(icon, leftIcon_left, iconTop, paint);
        icon = BitmapFactory.decodeResource(resources, R.mipmap.more);
        rightIcon_left = left + getFontLength(paint, month) + textPadding;
        canvas.drawBitmap(icon, rightIcon_left, iconTop, paint);*/
    }

    /**
     * 星期
     * @param canvas
     */
    private void drawWeek(Canvas canvas) {
        String[] weeks = {"日", "一", "二", "三", "四", "五", "六"};
        paint.setColor(0xffffff);
        RectF rect = new RectF(0, monthHeight, getWidth(), monthHeight + weekHeight);
        canvas.drawRect(rect, paint);
        RectF ovalF = new RectF(30, monthHeight, getWidth() - 30, monthHeight + weekHeight);
        paint.setColor(weekBgColor);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(ovalF, 10, 10, paint);
        Log.d("liuqi", "monthHeight = " + monthHeight + " weekHeight = " + weekHeight);

        paint.setTextSize(weekTextSize);
        paint.setColor(weekTextColor);
        paint.setStyle(Paint.Style.STROKE);
        columnWidth = getWidth() / 7;
        int baseline = monthHeight + (weekHeight + getFontHeight(paint)) / 2/* - offsetY * 2*/;
        for (int i = 0; i < weeks.length; i++) {
            String week = weeks[i];
            int weekWidth = getFontLength(paint, week);
            int left = i * columnWidth + (columnWidth - weekWidth) / 2;
            canvas.drawText(week, left, baseline, paint);
        }
    }

    private void drawDay(Canvas canvas) {
        int starNum = 0;
        int firstPos = 0;
        int top = monthHeight + weekHeight;
        for (int line = 0; line < lineNums; line++) {
            if(line == 0){
                //第一行
                //top += textPadding;
                starNum = 1;
                firstPos = firstLineIndex;
                drawDayUtil(canvas, top, firstLineNums, starNum, firstPos);
                /*for (int i = 0; i < firstLineNums; i++) {
                    int left = (firstLineIndex + i) * columnWidth + (columnWidth - getFontLength(paint, i + 1 + "")) / 2;
                    int baseline = top + (dayHeight + getFontHeight(paint)) / 2 - offsetY * 2 + offsetY * line;
                    canvas.drawText(i + 1+"", left, baseline, paint);
                }*/
            }else if(line == lineNums - 1){
                //最后一行
                //int leftNums = firstLineNums + (lineNums - 2) * 7 + 1;
                top += dayHeight;
                starNum = firstLineNums + (lineNums - 2) * 7 + 1;
                firstPos = 0;
                drawDayUtil(canvas, top, lastLineNums, starNum, firstPos);
                /*for (int i = 0; i < lastLineNums; i++) {
                    int left = i * columnWidth + (columnWidth - getFontLength(paint, leftNums + "")) / 2;
                    int baseline = top + line * dayHeight + (dayHeight + getFontHeight(paint)) / 2 - offsetY * 2 + offsetY * line;
                    canvas.drawText(leftNums + i + "", left, baseline, paint);
                }*/
            }else{
                //int num = firstLineNums + (line - 1) * 7 + 1;
                top += dayHeight;
                starNum = firstLineNums + (line - 1) * 7 + 1;
                firstPos = 0;
                drawDayUtil(canvas, top, WEEK_NUM, starNum, firstPos);
               /* for (int i = 0; i < WEEK_NUM; i++) {
                    int left = i * columnWidth + (columnWidth - getFontLength(paint, num + "")) / 2;
                    int baseline = top + line * dayHeight + (dayHeight + getFontHeight(paint)) / 2 - offsetY * 2 + offsetY * line;
                    canvas.drawText(num + i + "", left, baseline, paint);
                }*/
            }
        }
    }

    /**
     * @param top　顶部开始位置
     * @param canvas
     * @param linenum 这一行有多少个元素
     * @param startNum　从那个数字开始
     * @param firstPos  这行从哪个位置开始画
     */
    public void drawDayUtil(Canvas canvas, int top, int linenum, int startNum, int firstPos){
        int bottom = top + dayHeight;
        paint.setColor(dayBgColor);
        RectF rect = new RectF(0, top, getWidth(), bottom);
        canvas.drawRect(rect, paint);
        Log.d("liuqi", "top = " + top + " bottom = "+ bottom + " getWidth() = " + getWidth() + " startNum =" +startNum);

        paint.setTextSize(dayTextSize);
        paint.setColor(dayTextColor);
        for (int i = 0; i < linenum; i++) {
            paint.setTextSize(dayTextSize);
            int textLength = getFontLength(paint, startNum + i + "");
            int left = (firstPos + i) * columnWidth + (columnWidth - textLength) / 2;
            int baseline = top + (dayHeight + getFontHeight(paint)) / 2 /*- offsetY * 2 + offsetY * currentLine - textPadding*/;

            /*if(isCurrentMonth && currentDay == startNum + i){
                paint.setColor(0xffff0000);
                paint.setStyle(Paint.Style.STROKE);
                //虚线
                PathEffect effect = new DashPathEffect(new float[]{2, 1, 2, 1, 2, 1, 2, 1}, 1);
                paint.setStrokeWidth(3);
                paint.setPathEffect(effect);
                canvas.drawCircle(left + textLength / 2 , top + dayHeight / 2 + offsetY, radius, paint);
            }
            if(selectDay == startNum + i){
                paint.setColor(0xffff0000);
                paint.setStyle(Paint.Style.FILL);
                paint.setStrokeWidth(3);
                canvas.drawCircle(left + textLength / 2 , top + dayHeight / 2 + offsetY, radius, paint);
            }*/
            if(bgList != null && startNum + i <= bgList.size()){
                Boolean isSign = bgList.get(startNum + i - 1);
                if(isSign){
                    paint.setColor(signDayBgColor);
                    paint.setStyle(Paint.Style.FILL);
                    paint.setStrokeWidth(3);
                    canvas.drawCircle(left + textLength / 2 , top + dayHeight / 2 + offsetY, radius, paint);

                    paint.setColor(signDayTextColor);
                    paint.setStrokeWidth(0);
                    if(isCurrentMonth && currentDay == startNum + i){
                        //如果显示汉字需要微调保证居中
                        canvas.drawText("今", left - charOffset, baseline, paint);
                    }else{
                        canvas.drawText(startNum + i + "", left, baseline, paint);
                    }
                }else{
                    paint.setStyle(Paint.Style.FILL);
                    paint.setColor(dayTextColor);
                    paint.setStrokeWidth(0);
                    if(isCurrentMonth && currentDay == startNum + i){
                        //如果显示汉字需要微调保证居中
                        canvas.drawText("今", left - charOffset, baseline, paint);
                    }else {
                        canvas.drawText(startNum + i + "", left, baseline, paint);
                    }
                }
            }else {
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(dayTextColor);
                paint.setStrokeWidth(0);
                if(isCurrentMonth && currentDay == startNum + i){
                    //如果显示汉字需要微调保证居中
                    canvas.drawText("今", left - charOffset, baseline, paint);
                }else {
                    canvas.drawText(startNum + i + "", left, baseline, paint);
                }
            }
        }
    }

    /*@Override
    public boolean onTouchEvent(MotionEvent event) {
        PointF pointF = new PointF();
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        switch (action){
            case MotionEvent.ACTION_DOWN:
                pointF.set(event.getX(), event.getY());
                touch(pointF, false);
                break;
            case MotionEvent.ACTION_MOVE:
                pointF.set(event.getX(), event.getY());
                touch(pointF, false);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                pointF.set(event.getX(), event.getY());
                touch(pointF, true);
                break;
        }
        return true;
    }*/

    private void touch(PointF pointF, boolean isEnd) {
        if(isEnd && pointF.y >=0 && pointF.y <= monthHeight){
            Date date = string2Date(mMonth);
            mCalendar.setTime(date);
            if(pointF.x >= leftIcon_left && pointF.x <= leftIcon_left + iconAreaWidth){
                preMonth();
            }else if(pointF.x >= rightIcon_left && pointF.x <= rightIcon_left + iconAreaWidth){
                nextMonth();
            }
        }else if(isEnd && pointF.y >= monthHeight + weekHeight){
            float x = pointF.x;
            float y = pointF.y - monthHeight - weekHeight - textPadding;
            int xIndex = (int) (x / columnWidth);
            int yIndex = (int) (y / dayHeight);
            xIndex += 1;
            if(xIndex <= 0){
                xIndex = 1;
            }
            if(xIndex > 7){
                xIndex = 7;
            }
            if(yIndex == 0){
                if(xIndex < firstLineIndex){
                    return;
                }else{
                    selectDay = xIndex - firstLineIndex;
                    invalidate();
                }
            }else if(yIndex == lineNums - 1){
                if(xIndex > lastLineNums){
                    return;
                }else{
                    selectDay = firstLineNums + (yIndex - 1) * WEEK_NUM + xIndex;
                    invalidate();
                }
            }else{
                selectDay = firstLineNums + (yIndex - 1) * WEEK_NUM + xIndex;
                invalidate();
            }
        }
    }

    public void preMonth(){
        mCalendar.add(Calendar.MONTH, -1);
        mCalendar.set(Calendar.DATE, 1);
        mMonth = date2String(mCalendar.getTime());
        setMonth(mMonth);
        requestLayout();
        invalidate();
    }

    public void nextMonth(){
        mCalendar.add(Calendar.MONTH, 1);
        mCalendar.set(Calendar.DATE, 1);
        mMonth = date2String(mCalendar.getTime());
        setMonth(mMonth);
        requestLayout();
        invalidate();
    }

    /**
     * 字符串格式转化为日期
     * @param month
     * @return
     */
    public Date string2Date(String month){
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月");
        Date date = null;
        try {
            date = format.parse(month);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 获取对应字体大小所需的高度
     * @param paint
     * @return
     */
    public int getFontHeight(Paint paint){
        return (int) (paint.getFontMetrics().descent - paint.getFontMetrics().ascent);
    }

    /**
     * 获取字符宽度
     * @param paint
     * @param text
     * @return
     */
    public int getFontLength(Paint paint, String text){
        Rect rect = new Rect();
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.width();
    }
}
