package com.sign.signcalendar.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;


import com.sign.signcalendar.R;

import java.io.IOException;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * 包含gif动画显示的dilog框
 * six.sev
 */
public class GifDialogHelper {
	public static final String TAG = "GifDialogHelper";
	private static final int MAX_WIDTH = 320;


	/**
	 * 创建对话框的辅助类
	 *
	 * @author HuQiming
	 * @date 2013-6-20
	 *
	 */
	public static class Builder {
		private String msg;

		private Handler mHanlder;

		private int gifResId = -1;

		private boolean isCloseOnTouchOutside;
		private DialogInterface.OnDismissListener dismissListener;

		private AnimationCompleteListener animationCompleteListener;


		public Builder setMsg(String msg) {
			this.msg = msg;
			return this;
		}

		public Builder setHanlder(Handler handler){
			this.mHanlder = handler;
			return this;
		}

		public Builder setGifResId(int resId){
			this.gifResId = resId;
			return this;
		}

		public Builder setDismissListener(DialogInterface.OnDismissListener dismissListener){
			this.dismissListener = dismissListener;
			return this;
		}

		public Builder setCloseOnTouchOutside(boolean isCloseOnTouchOutside) {
			this.isCloseOnTouchOutside = isCloseOnTouchOutside;
			return this;
		}

		public Builder setAnimationCompleteListener(AnimationCompleteListener animationCompleteListener){
			this.animationCompleteListener = animationCompleteListener;
			return this;
		}

		public Dialog show(Context ctx) {
			return showDialog(ctx, this);
		}
	}


	public static Dialog showDialog(Context ctx, final Builder builder) {
		if (ctx == null || builder == null) {
			return null;
		}

		final Dialog dialog = new Dialog(ctx);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
		View contentView = LayoutInflater.from(ctx).inflate(R.layout.dialog_gif, null);
		GifImageView gifImageView = (GifImageView) contentView.findViewById(R.id.gifView);
		final TextView textDes = (TextView) contentView.findViewById(R.id.discription);
		int gifId = builder.gifResId;
		final String message = builder.msg;
		final Handler handler = builder.mHanlder;
		DialogInterface.OnDismissListener dismissListener = builder.dismissListener;
		final AnimationCompleteListener animationCompleteListener = builder.animationCompleteListener;
		if(gifId != -1){
			try {
				final GifDrawable drawable = new GifDrawable(ctx.getResources(), gifId);
				drawable.addAnimationListener(new AnimationListener() {
					@Override
					public void onAnimationCompleted(int loopNumber) {
						drawable.stop();
						if(message != null){
							textDes.setText(message);
						}
						if(animationCompleteListener != null){
							animationCompleteListener.animationComplete();
						}
						if(handler != null){
							handler.postDelayed(new Runnable() {
								@Override
								public void run() {
									dialog.dismiss();
								}
							}, 1000);
						}
					}
				});
				gifImageView.setImageDrawable(drawable);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		if(dismissListener != null){
			dialog.setOnDismissListener(dismissListener);
		}
		dialog.setContentView(contentView);

		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		DisplayMetrics dm = ctx.getResources().getDisplayMetrics();
		int dw = dm.widthPixels < dm.heightPixels ? dm.widthPixels
				: dm.heightPixels;
		float den = dm.density;
		int max = (int) (MAX_WIDTH * den);
		int w = (dw * 8) / 9;
		int width = (w < max) ? w : max;
		dialog.getWindow().setLayout(width,
				WindowManager.LayoutParams.WRAP_CONTENT);
		dialog.getWindow().getDecorView().requestLayout();

		if (ctx instanceof Activity) {
			Activity activity = (Activity) ctx;
			if (activity.isFinishing()) {
				// Activity销毁状态的时候，show dialog会报错
				return dialog;
			}
		}
		dialog.show();
		return dialog;
	}

	public interface AnimationCompleteListener{
		public void animationComplete();
	}
}
