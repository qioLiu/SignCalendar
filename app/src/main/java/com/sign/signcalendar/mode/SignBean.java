package com.sign.signcalendar.mode;


import java.io.Serializable;
import java.util.List;

/**
 * 签到实体类
 * Created by six.sev on 2017/5/25.
 */

public class SignBean implements Serializable {
    //是否第一次签到
    private boolean isFirstSignin;
    //返回已签到过的日期列表
    private List<SignObjectBean> list;
    //备注
    private String remark;
    //最新总积分
    private int sumScore;
    //签到成功提示
    private String tips;
    //签到前的总积分
    private int yesterdaySumScore;


    public boolean isFirstSignin() {
        return isFirstSignin;
    }

    public void setFirstSignin(boolean firstSignin) {
        isFirstSignin = firstSignin;
    }

    public List<SignObjectBean> getList() {
        return list;
    }

    public void setList(List<SignObjectBean> list) {
        this.list = list;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getSumScore() {
        return sumScore;
    }

    public void setSumScore(int sumScore) {
        this.sumScore = sumScore;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public int getYesterdaySumScore() {
        return yesterdaySumScore;
    }

    public void setYesterdaySumScore(int yesterdaySumScore) {
        this.yesterdaySumScore = yesterdaySumScore;
    }

    public static class SignObjectBean implements Serializable{
        private String createTime;
        private int score;

        public SignObjectBean(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
