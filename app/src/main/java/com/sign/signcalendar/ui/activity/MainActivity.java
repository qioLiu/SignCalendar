package com.sign.signcalendar.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.sign.signcalendar.R;
import com.sign.signcalendar.mode.SignBean;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView tv1;
    private SignBean signBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = (TextView) findViewById(R.id.tv1);
        initData();
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goCalendar();
            }
        });
    }

    private void initData() {
        //模拟初始化数据 实际从后台获取。
        signBean = new SignBean();
        signBean.setYesterdaySumScore(50);
        signBean.setRemark("今日签到成功，获取5积分");
        signBean.setSumScore(55);
        signBean.setFirstSignin(true);
        signBean.setTips("今日签到成功，获取5积分");
        /**
         * 获取的日期list的格式请根据自己的后台需求去商定对接
         * 目前采用字符串的list， 在signActivity中做了一个重新归纳
         */
        List<SignBean.SignObjectBean> list = new ArrayList<SignBean.SignObjectBean>();
        for (int i = 0; i < 5; i++) {
            switch (i){
                case 0:
                    list.add(new SignBean.SignObjectBean("2017-05-11"));
                    break;
                case 1:
                    list.add(new SignBean.SignObjectBean("2017-05-12"));
                    break;
                case 2:
                    list.add(new SignBean.SignObjectBean("2017-06-13"));
                    break;
                case 3:
                    list.add(new SignBean.SignObjectBean("2017-06-14"));
                    break;
                case 4:
                    list.add(new SignBean.SignObjectBean("2017-06-15"));
                    break;
                default:
                    list.add(new SignBean.SignObjectBean("2017-07-10"));
                    break;
            }
        }
        signBean.setList(list);
    }

    private void goCalendar() {
        Intent intent = new Intent(this, SignActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("signBean", signBean);
        intent.putExtra("signBundle", bundle);
        startActivity(intent);
    }
}
