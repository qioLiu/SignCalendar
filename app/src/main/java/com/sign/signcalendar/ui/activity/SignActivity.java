package com.sign.signcalendar.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sign.signcalendar.R;
import com.sign.signcalendar.adapter.CalendarPagerAdapter;
import com.sign.signcalendar.mode.SignBean;
import com.sign.signcalendar.ui.fragment.CurMonthFragment;
import com.sign.signcalendar.ui.fragment.NextMonthFragment;
import com.sign.signcalendar.ui.fragment.PreMonthFragment;
import com.sign.signcalendar.widget.GifDialogHelper;
import com.sign.signcalendar.widget.RiseNumberTextView;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 签到页面
 * Created by six.sev on 2017/5/23.
 */

public class SignActivity extends AppCompatActivity {

    //当前月的前一个月
    private PreMonthFragment mPreFragment;
    //当前月
    private CurMonthFragment mCurFragment;
    //后一个月
    private NextMonthFragment mNextFragment;
    //存储页面的数组
    List<Fragment> fragments;

    //是否当天第一次签到
    private boolean isFirstSign = false;
    //
    private int yesterdayScore;
    private int sumScore;
    private String tip;
    private String remark;
    private List<SignBean.SignObjectBean> data;
    /**
     * 经过归纳后的按月份排列的列表
     */
    HashMap<Integer, List<Boolean>> newData = new HashMap<Integer, List<Boolean>>();
    private List<Boolean> preList;
    private List<Boolean> curList;
    private List<Boolean> nextList;
    private String mPreMonthString;
    private String mCurMonthString;
    private String mNextMonthString;

    Calendar mCalendar;

    ImageView indicator_select;
    ImageView indicator_empty1;
    ImageView indicator_empty2;
    LinearLayout indicatorContainer;
    ViewPager mViewPager;
    TextView mTextmonth;
    TextView mTextTip;
    RiseNumberTextView mTextScore;
    SignBean signBean;
    Handler mHandler = new Handler();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        indicatorContainer = (LinearLayout) findViewById(R.id.indicator_container);
        mTextmonth = (TextView) findViewById(R.id.monthString);
        mTextScore = (RiseNumberTextView) findViewById(R.id.score);
        mTextTip = (TextView) findViewById(R.id.tip);
        initViews();
        mHandler.postDelayed(new MyRunnable(SignActivity.this), 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    public void initViews(){
        mCalendar = Calendar.getInstance();
        initIndicator();

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i){
                    //当前月的前一个月
                    case 0:
                        mTextmonth.setText(mPreMonthString);
                        break;
                    //当前月
                    case 1:
                        mTextmonth.setText(mCurMonthString);
                        break;
                    //当前月的后一个月
                    case 2:
                        mTextmonth.setText(mNextMonthString);
                        break;
                }
                showIndicator(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    public void initData(){
        signBean = (SignBean) getIntent().getBundleExtra("signBundle").getSerializable("signBean");
        if(signBean != null){
            isFirstSign = signBean.isFirstSignin();
            yesterdayScore = signBean.getYesterdaySumScore();
            sumScore = signBean.getSumScore();
            tip = signBean.getTips();
            remark= signBean.getRemark();
            data = signBean.getList();
            mapData(data);
            initUI();
        }
    }


    public void initUI(){
        //进去显示积分和提示 根据情况是否显示动画
        showScoreAndTip();
        //初始化日历数据
        initCalendarData();

        fragments = new ArrayList<Fragment>();
        mPreFragment = new PreMonthFragment(mPreMonthString, preList);
        fragments.add(mPreFragment);
        mCurFragment = new CurMonthFragment(mCurMonthString, curList);
        fragments.add(mCurFragment);
        mNextFragment = new NextMonthFragment(mNextMonthString, nextList);
        fragments.add(mNextFragment);
        CalendarPagerAdapter adapter = new CalendarPagerAdapter(getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(1);

        showIndicator(1);
    }

    /**
     * //进去显示积分和提示 根据情况是否显示动感画
     */
    public void showScoreAndTip(){
        if(isFirstSign){
            mTextScore.setText(yesterdayScore + "");
            new GifDialogHelper.Builder().setGifResId(R.mipmap.sign_loading)
                    .setMsg(tip)
                    .setHanlder(new Handler())
                    .setDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            //字体滚动
                            mTextScore.withNumber(yesterdayScore, sumScore);
                            mTextScore.setDuration(1000);
                            mTextScore.setOnEndListener(new RiseNumberTextView.EndListener() {
                                @Override
                                public void onEndFinish() {
                                    mTextScore.setText(sumScore + "");
                                }
                            });
                            mTextScore.start();
                            mTextTip.setText(remark);
                        }
                    })
                    .show(this);
        }else{
            mTextScore.setText(sumScore + "");
            mTextTip.setText(remark);
        }
    }

    /**
     * v//初始化日历数据
     */
    public void initCalendarData(){
        //当前时间对应的月份
        Date date = new Date();
        mCalendar.setTime(date);
        mCurMonthString = date2String(date);
        int curMonth = mCalendar.get(Calendar.MONTH);
        curList = newData.get(curMonth);
        //前一个月
        mCalendar.add(Calendar.MONTH, -1);
        mCalendar.set(Calendar.DATE, 1);
        int preMonth = mCalendar.get(Calendar.MONTH);
        mPreMonthString = date2String(mCalendar.getTime());
        preList = newData.get(preMonth);
        //后一个月
        mCalendar.add(Calendar.MONTH, 2);
        mCalendar.set(Calendar.DATE, 1);
        int nextMonth = mCalendar.get(Calendar.MONTH);
        mNextMonthString = date2String(mCalendar.getTime());
        nextList = newData.get(nextMonth);
    }


    /**
     * 根据后台返回的所有日期的列表来做一个重新的分类得到最新的列表数据 根据月份来排列
     * @param data
     * @return
     */
    private void mapData(List<SignBean.SignObjectBean> data) {
        int length = data.size();
        for (int i = 0; i < length; i++) {
            SignBean.SignObjectBean signObject = data.get(i);
            String dateString = signObject.getCreateTime();
            Date date = string2Date(dateString);
            mCalendar.setTime(date);
            int month = mCalendar.get(Calendar.MONTH);
            int day = mCalendar.get(Calendar.DAY_OF_MONTH);
            int maxDay = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            if(newData.containsKey(month)){
                List<Boolean> keyData = newData.get(month);
                for (int j = 0; j < keyData.size(); j++) {
                    if(j == day - 1){
                        keyData.set(j, true);
                        break;
                    }
                }
            }else{
                List<Boolean> datas = new ArrayList<Boolean>();
                for (int j = 0; j < maxDay; j++) {
                    if(j == day - 1) {
                        datas.add(true);
                    }else{
                        datas.add(false);
                    }
                }
                newData.put(month, datas);
            }
        }
    }

    /**
     * 字符串格式转化为日期
     * @param dateString
     * @return
     */
    public Date string2Date(String dateString){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 获取的系统时间转换为我们需要的字符串格式
     * @param date
     * @return
     */
    public String date2String(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月");
        return format.format(date);
    }

    /**
     * 初始化三个小圆球
     */
    public void initIndicator(){
        indicator_select = new ImageView(this);
        indicator_select.setImageResource(R.drawable.indicator_select);
        indicatorContainer.addView(indicator_select);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) indicator_select.getLayoutParams();
        layoutParams.setMargins(12, 12, 12, 12);
        indicator_select.setLayoutParams(layoutParams);

        indicator_empty1 = new ImageView(this);
        indicator_empty1.setImageResource(R.drawable.indicator_empty);
        indicatorContainer.addView(indicator_empty1);
        LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) indicator_select.getLayoutParams();
        layoutParams.setMargins(12, 12, 12, 12);
        indicator_empty1.setLayoutParams(layoutParams1);

        indicator_empty2 = new ImageView(this);
        indicator_empty2.setImageResource(R.drawable.indicator_empty);
        indicatorContainer.addView(indicator_empty2);
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) indicator_select.getLayoutParams();
        layoutParams.setMargins(12, 12, 12, 12);
        indicator_empty2.setLayoutParams(layoutParams2);
    }

    /**
     * 显示几个小圆球的顺序
     * @param position
     */
    public void showIndicator(int position){
        indicatorContainer.removeAllViews();
        switch (position){
            case 0:
                indicatorContainer.addView(indicator_select);
                indicatorContainer.addView(indicator_empty1);
                indicatorContainer.addView(indicator_empty2);
                break;
            case 1:
                indicatorContainer.addView(indicator_empty1);
                indicatorContainer.addView(indicator_select);
                indicatorContainer.addView(indicator_empty2);
                break;
            case 2:
                indicatorContainer.addView(indicator_empty1);
                indicatorContainer.addView(indicator_empty2);
                indicatorContainer.addView(indicator_select);
                break;
            default:
                indicatorContainer.addView(indicator_empty1);
                indicatorContainer.addView(indicator_select);
                indicatorContainer.addView(indicator_empty2);
                break;
        }
    }

    /**
     * 静态内部类防止内存泄漏
     */
    static class MyRunnable implements Runnable{
        WeakReference<Activity> wr;

        public MyRunnable(Activity context) {
            wr = new WeakReference<Activity>(context);
        }

        @Override
        public void run() {
            SignActivity context = (SignActivity) wr.get();
            if(context != null){
                context.initData();
            }
        }
    }
}
