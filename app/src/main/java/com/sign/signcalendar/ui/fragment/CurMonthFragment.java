package com.sign.signcalendar.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sign.signcalendar.R;
import com.sign.signcalendar.widget.CustomCalendarView2;

import java.util.List;

/**
 * 签到的当前月份
 * Created by six.sev on 2017/5/25.
 */

@SuppressLint("ValidFragment")
public class CurMonthFragment extends Fragment {
    CustomCalendarView2 preMonth;

    private String montString;
    private List<Boolean> bgList;

    public CurMonthFragment(String montString, List<Boolean> bgList) {
        this.montString = montString;
        this.bgList = bgList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pre_month, null);
        preMonth = (CustomCalendarView2) view.findViewById(R.id.preMonth);
        preMonth.setMonth(montString);
        preMonth.setDayBgList(bgList);
        return view;
    }

}
